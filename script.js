document.getElementById('search-input').addEventListener('keyup', async (e) => {
    // Search comments
    // Use this API: https://jsonplaceholder.typicode.com/comments?postId=3
    // Display the results in the UI

    // Things to look out for
    // ---
    // Use es6
    // Error handling

    if (e.target.value !== "") {
        try {

          const response = await fetch('http://localhost:8000/');

          if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }

          const data = await response.json();
      
          const results = data
            .filter(item => item.name.includes(e.target.value))
            .map(item => `<li class="list-group-item">${item.name}</li>`)
            .join("");
      
          document.getElementById('results').innerHTML = results;

        } catch (error) {
          console.error(error);
        }

    } else {
        document.getElementById('results').innerHTML = "";
    }
      

})