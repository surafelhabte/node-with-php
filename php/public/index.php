<?php
/**
 * https://www.php.net/manual/en/curl.examples-basic.php
 */

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");

$url = 'https://jsonplaceholder.typicode.com/comments?postId=3';

try {

    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $data = curl_exec($ch);

    curl_close($ch);

    if (!$data) {
        throw new Exception("Failed to fetch data from the URL: $url");
    }
    echo $data;

} catch (Exception $e) {
    http_response_code(500);
    echo json_encode(array('message' => 'Error: ' . $e->getMessage()));
}

?>