import('node-php-server')
  .then((phpServer) => {
    phpServer.createServer({
        port: 8000,
        hostname: '127.0.0.1',
        base: '.',
        keepalive: false,
        open: false,
        bin: 'php',
        router: __dirname + '/server.php'
    });
  })
  .catch((error) => {
    console.error('Failed to load node-php-server', error);
  });